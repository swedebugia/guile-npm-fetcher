;;; Copyright © 2018 Julien Lepiller <julien@lepiller.eu>
;;; Copyright © 2018 swedebugia <swedebugia@riseup.net>
;;;
;;; This file is part of guile-npm-explorer.
;;;
;;; guile-npm-explorer is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guile-npm-explorer is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-npm-explorer.  If not, see <http://www.gnu.org/licenses/>.

;; Usage:
;; $guile -s npm-explorer.scm >mocha.dot (later you pipe these
;; dot-files into graphviz to produce the actual graph.
;;
;; or
;;
;; Do it all at once:
;; guile -s npm-explorer.scm |dot -Tsvg > mocha.svg
;;
;; or
;;
;; Do it all at once and show it with no nonsense in between:
;; guile -s npm-explorer.scm |dot -Tsvg > mocha.svg && eog mocha.svg

(use-modules (guix import json)
	     (guix build utils)
	     (guix import utils)
	     (guix http-client)
	     (srfi srfi-34)
	     (srfi srfi-1)
	     (ice-9 hash-table)
	     (ice-9 rdelim)
	     (ice-9 regex)
	     (ice-9 textual-ports)
	     (json))

;; from
;; https://gitlab.com/swedebugia/guix/blob/08fc0ec6fa76d95f4b469aa85033f1b0148f7fa3/guix/import/npm.scm
;; imported here unchanged because it is not avaliable in upstream guix yet.
(define (node->package-name name)
    "Given the NAME of a package on npmjs, return a Guix-compliant
name for the
package. We remove the '@' and keep the '/' in scoped
packages. E.g. @mocha/test -> node-mocha/test"
    (cond ((and (string-prefix? "@" name)
		(string-prefix? "node-" name))
	   (snake-case (string-drop name 1)))
	  ((string-prefix? "@" name)
	            (string-append "node-" (snake-case (string-drop
							name 1))))
	  ((string-prefix? "node-" name)
	   (snake-case name))
	  (else
	   (string-append "node-" (snake-case name)))))

(define (slash->_ name)
  "Sanitize slashes to avoid cli-problems"
  (if (string-match "[/]" name)
      (regexp-substitute #f (string-match "/+" name)
			 'pre "_slash_" 'post)
      ;;else
      name))

;; FIXME this does not return #f if the file is empty.
(define (read-file file)
  "RETURN hashtable from JSON-file in cache."
  (if (< (stat:size (stat file)) 10)
      ;; size is less than 10 bytes, return #f
      #f
      ;; return file parsed to hashtables with (json)
      (call-with-input-file file
	(lambda (port)
	  (json->scm port)))))

;; from
;; http://git.savannah.gnu.org/cgit/guix.git/tree/guix/import/json.scm
;; adapted to return unaltered JSON
(define* (npm-http-fetch url
                     ;; Note: many websites returns 403 if we omit a
                     ;; 'User-Agent' header.
                     #:key (headers `((user-agent . "GNU Guile")
                                      (Accept . "application/json"))))
  "Return a JSON resource URL, or
#f if URL returns 403 or 404.  HEADERS is a list of HTTP headers to pass in
the query."
  (guard (c ((and (http-get-error? c)
                  (let ((error (http-get-error-code c)))
                    (or (= 403 error)
                        (= 404 error))))
             #f))
    (let* ((port   (http-fetch url #:headers headers))
	      ;; changed the upstream here to return unaltered json:
              (result (get-string-all port)))
      (close-port port)
      result)))

(define (cache-handler name)
  "Check if cached in cache-dir. RETURN direct from cache or fetch and return
from cache."
  (let* ((cache-dir (string-append (getenv "HOME") "/.cache/npm-explorer"))
	 ;; sanitize name to fit in cli-context on disk
	 ;; it can contain @ and /
	 (cache-name (slash->_ (node->package-name name)))
	 (filename (string-append cache-dir "/" cache-name ".package.json")))
    (if (file-exists? filename)
	;;yes
	;;check if empty
	(if (read-file filename)
	    (read-file filename)
	    ;;file empty
	    (begin
	      (format
	       (current-error-port)
	       "cache for ~a was empty, trying to download again..." name)
	      (delete-file filename)
	      ;; call handler again to try fetching again
	      (cache-handler name)))
	;;no
	(begin
	  (when (not (directory-exists? cache-dir))
	    (mkdir-p cache-dir))
	  ;; port closes when this closes
	  (call-with-output-file filename
	    (lambda (port)
	      (format port "~a"
		      ;; this gives os the result-closure and we write it out
		      (npm-http-fetch
		       (string-append
			"https://registry.npmjs.org/"
			name)))))
	  ;; get the content and close
	  (read-file filename)))))

;; FIXME implement returning the latest version if ^ is prefixed to
;; avoid us having to import lots of old versions.
(define (sanitize-version version)
  (cond
   ((string-prefix? "^" version)
    (string-drop version 1))
   ((string-prefix? "*" version)
    (string-drop version 1))
   (else
    version)))

(define (extract-version hashtable version)
  "Return extract from hashtable corresponding to version or #f if not
found."
  (cond
   ((or
     (equal? version "latest")
     (equal? version "*"))
    (let ((latest (hash-ref (hash-ref hashtable "dist-tags") "latest")))
      (hash-ref (hash-ref hashtable "versions") latest)))
   (else
    ;;extract the version specified
    (hash-ref (hash-ref hashtable "versions") version))))

(define (extract-deps hashtable)
  "Return hashtable with extract of dependencies and devdependencies from
hashtable corresponding to the latest version."
  (let* ((latest (hash-ref (hash-ref hashtable "dist-tags") "latest"))
	 (data (hash-ref (hash-ref hashtable "versions") latest)))
    ;; Some packages have no deps or devdeps
    (cond
     ((and (equal? (hash-ref data "dependencies") #f)
	   (hash-table? (hash-ref data "devDependencies")))
      ;;return devdeps only
      (hash-ref data "devDependencies"))
     ((and (hash-table? (hash-ref data "dependencies"))
	   (equal? (hash-ref data "devDependencies") #f))
      ;;return deps only
      (hash-ref data "dependencies"))
     ((and (equal? #f (hash-table? (hash-ref data "dependencies")))
	   (equal? #f (hash-table? (hash-ref data "devDependencies"))))
      ;; npm strangeness - no dependencies at all
      ;; e.g. angular returns this
      ;;return false
      #f)
     (else
      ;;debug
      ;; (display "dep")
      ;; (display	(hash-table? (hash-ref data "dependencies")))
      ;; (display "devdep")
      ;; (display	(hash-table? (hash-ref data "devDependencies")))      
      ;; Combine the two hashtables to one
      (let ((dep (hash-map->list cons (hash-ref data "dependencies")))
	    (ddep (hash-map->list cons (hash-ref data "devDependencies"))))
	(alist->hash-table (append dep ddep))))

      )))

;;(display (extract-deps (cache-handler "angular")))

(begin
  (define (fetch-json name done level)
    (cond
     ((member name done)
      done)
     ;; These give errors for some reason
     ((member name '("buble" "angular" "karma-mocha" "acorn-bigint" "test262" "websocket-server"))
      done)
     (else
      ;; Convert return from cache to hashtable instead of fetching
      ;; everything multiple times for packages with shared dependency
      ;; tails. This results in a significant speedup when file is in
      ;; the cache.
      ;; The cache has no TTL implemented so you should clear it from
      ;; to time manually.
      (let* ((pkg (cache-handler name))
	     (deps (extract-deps pkg)))
	(if deps
	    (catch #t
	      ;; Thunk
	      (lambda ()
		;; Fold through all the elements in the hashtable
	    	(hash-fold
	    	 (lambda (key value acc)
	    	   (begin
	    	     (format (current-output-port)
	    		     "name: ~a level ~a: ~a packages    \n"
			     name level
			     (length acc))
		     #;
		     (format (current-output-port) ; ;
	    	     "name ~a\n" name)
		     ;; Continue recursively
	    	     (fetch-json key acc (+ 1 level))))
		 ;; fold recursive
	    	 (cons name done) deps)
		;; else, add to done
	    	(cons name done))
	      ;; Handler if thunk throws #t
	      ;; not found!
	      (lambda _
		(error
		 (string-append
		  "unable to find json-data for the last lookup"  " in the registry!"))
		;; continue
		(cons name done)
		))
	    ;; else
	    (cons name done))))))

  (fetch-json "ssb-server" '() 0))

